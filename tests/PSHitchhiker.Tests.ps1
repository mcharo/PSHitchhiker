# Import our module to use InModuleScope
Import-Module (Resolve-Path ".\PSHitchhiker\PSHitchhiker.psd1") -Force

# Use InModuleScope to gain access to private functions
InModuleScope "PSHitchhiker" {
    Describe "Public/Ask" { # "Folder/File"
        It "is true" {
            Invoke-PSHitchhiker | Should Be 42
        }
    }
}