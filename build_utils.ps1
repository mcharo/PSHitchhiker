<#
.SYNOPSIS
    Publishes a PowerShell Module to a Network Share.

.Example
    $ModuleInfo = @{
        RepoName   = 'PoshRepo'
        RepoPath   = '\\server\PoshRepo'
        ModuleName = 'BuildHelpersTest'
        ModulePath = '.\BuildHelpersTest.psd1'
    }

    Publish-SMBModule @ModuleInfo
#>
function Publish-SMBModule
{
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory=$true)]
        [string] $RepoName,

        [Parameter(Mandatory=$true)]
        [string] $RepoPath,

        [Parameter(Mandatory=$true)]
        [string] $ModuleName,

        [Parameter(Mandatory=$true)]
        [string] $ModulePath,

        [Parameter(Mandatory=$true)]
        [int] $BuildNumber
    )

    # Just force it x.x
    Install-Nuget

    # Resister SMB Share as Repository
    Write-Verbose ("Checking if Repo: {0} is registered" -f $RepoName)
    if(!(Get-PSRepository -Name $RepoName -ErrorAction SilentlyContinue))
    {
        Write-Verbose ("Registering Repo: {0}" -f $RepoName)
        Register-PSRepository -Name $RepoName -SourceLocation $RepoPath -InstallationPolicy Trusted
    }

    # Update Existing Manifest
    # - Source Manifest controls Major/Minor
    # - Jenkins Controls Build Number.
    Write-Verbose ("Checking if Module: {0} is registered" -f $ModuleName)
    if(Find-Module -Repository $RepoName -Name $ModuleName -ErrorAction SilentlyContinue)
    {
        Write-Verbose ("Updating Manifest for: {0}" -f $ModuleName)
        $version = (Get-Module -FullyQualifiedName $ModulePath -ListAvailable).Version | Select-Object Major, Minor
        $newVersion = New-Object Version -ArgumentList $version.major, $version.minor, $BuildNumber
        Update-ModuleManifest -Path $ModulePath -ModuleVersion $newVersion
    }

    # Publish ModuleInfo
    # - Fails if nuget install needs confirmation in NonInteractive Mode.
    Write-Verbose ("Publishing Module: {0}" -f $ModuleName)
    try
    {
        $env:PSModulePath += ";$PSScriptRoot"
        Publish-Module -Repository $RepoName -Name $ModuleName
    }
    catch [System.Exception]
    {
        # Write-Error "Publish Failed"
        throw($_.Exception)
    }
}

<#
    .SYNOPSIS
        Create a Zip Archive Build Artifact
#>
function Publish-ArtifactZip
{
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory=$true)]
        [string] $ModuleName,

        [Parameter(Mandatory=$true)]
        [int] $BuildNumber
    )

    # Creating project artifact
    $artifactDirectory = Join-Path $pwd "artifacts"
    $moduleDirectory = Join-Path $pwd "$ModuleName"
    $manifest = Join-Path $moduleDirectory "$ModuleName.psd1"
    $zipFilePath = Join-Path $artifactDirectory "$ModuleName.zip"

    $version = (Get-Module -FullyQualifiedName $manifest -ListAvailable).Version | Select-Object Major, Minor
    $newVersion = New-Object Version -ArgumentList $version.major, $version.minor, $BuildNumber
    Update-ModuleManifest -Path $manifest -ModuleVersion $newVersion

    Add-Type -assemblyname System.IO.Compression.FileSystem
    [System.IO.Compression.ZipFile]::CreateFromDirectory($moduleDirectory, $zipFilePath)
}

<#
    .SYNOPSIS
        Create a Nuget Package for the Build Artifact
        Simple wrapper around DscResourceTestHelper Module
#>
function Publish-NugetPackage
{
   param
    (
        [Parameter(Mandatory=$true)]
        [string] $packageName,
        [Parameter(Mandatory=$true)]
        [string] $author,
        [Parameter(Mandatory=$true)]
        [int] $BuildNumber,
        [Parameter(Mandatory=$true)]
        [string] $owners,
        [string] $licenseUrl,
        [string] $projectUrl,
        [string] $iconUrl,
        [string] $packageDescription,
        [string] $releaseNotes,
        [string] $tags,
        [Parameter(Mandatory=$true)]
        [string] $destinationPath
    )

    $CurrentVersion = (Get-Module -FullyQualifiedName "./$ModuleName" -ListAvailable).Version | Select-Object Major, Minor
    $version = New-Object Version -ArgumentList $CurrentVersion.major, $CurrentVersion.minor, $BuildNumber

    $moduleInfo = @{
        packageName = $packageName
        version =  ($version.ToString())
        author =  $author
        owners = $owners
        licenseUrl = $licenseUrl
        projectUrl = $projectUrl
        packageDescription = $packageDescription
        tags = $tags
        destinationPath = $destinationPath
    }

    # Creating NuGet package artifact
    Import-Module -Name DscResourceTestHelper
    New-Nuspec @moduleInfo

    $nuget = "$env:ProgramData\Microsoft\Windows\PowerShell\PowerShellGet\nuget.exe"
    . $nuget pack "$destinationPath\$packageName.nuspec" -outputdirectory $destinationPath
}

<#
    .SYNOPSIS
        Used to address problem running Publish-Module in NonInteractive Mode when Nuget is not present.

    .NOTES
        https://github.com/OneGet/oneget/issues/173
        https://github.com/PowerShell/PowerShellGet/issues/79

        If Build Agent does not have permission to ProgramData Folder, may want to use the user specific folder.

        Package Provider Expected Locations (x86):
            C:\Program Files (x86)\PackageManagement\ProviderAssemblies
            C:\Users\{USER}\AppData\Local\PackageManagement\ProviderAssemblies
#>
function Install-Nuget
{
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory=$false)]
        [switch] $Force = $false
    )

    # Force Update Provider
    Install-PackageProvider Nuget -Force

    $sourceNugetExe = "http://nuget.org/nuget.exe"
    $powerShellGetDir = "$env:ProgramData\Microsoft\Windows\PowerShell\PowerShellGet"

    if(!(Test-Path -Path $powerShellGetDir))
    {
        New-Item -ItemType Directory -Path $powerShellGetDir -Force
    }

    if(!(Test-Path -Path "$powerShellGetDir\nuget.exe") -or $Force)
    {
        Invoke-WebRequest $sourceNugetExe -OutFile "$powerShellGetDir\nuget.exe"
    }
}

<#
    .SYNOPSIS
        Quick Attempt to create a HTML page to publish during CI builds
#>
function Publish-CoverageHTML
{
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory=$true)]
        [PSCustomObject] $TestResults,

        [Parameter(Mandatory=$true)]
        [int] $BuildNumber,

        [Parameter(Mandatory=$true)]
        [string] $Repository,

        [Parameter(Mandatory=$true)]
        [int] $PercentCompliance,

        [Parameter(Mandatory=$true)]
        [string] $OutputFile
    )

    $HitTable = $TestResults.CodeCoverage.HitCommands | ConvertTo-Html -Fragment
    $MissedTable = $TestResults.CodeCoverage.MissedCommands | ConvertTo-Html -Fragment
    $CommandsAnalyzed = $TestResults.CodeCoverage.NumberOfCommandsAnalyzed
    $FilesAnalyzed = $TestResults.CodeCoverage.NumberOfFilesAnalyzed
    $CommandsExecuted = $TestResults.CodeCoverage.NumberOfCommandsExecuted
    $CommandsMissed = $TestResults.CodeCoverage.NumberOfCommandsMissed

    $CoveragePercent = [math]::Round(($CommandsExecuted / $CommandsAnalyzed * 100), 2)
    $Date = (Get-Date)

    $AnalyzedFiles = "";
    foreach ($file in $TestResults.CodeCoverage.analyzedfiles)
    {
        $AnalyzedFiles += "<li>$file</li>"
    }

    $BuildColor = 'green'
    if($CoveragePercent -lt $PercentCompliance)
    {
        $BuildColor = 'red'
    }

    $html = "
    <!DOCTYPE html>
    <html lang='en'>
    <head>
        <meta charset='UTF-8'>
        <title>Document</title>
        <style>
            body{font-family: sans-serif;}
            h2{margin: 0;}
            table{width: 100%;text-align: left;border-collapse: collapse;margin-top: 10px;}
            table th, table td {padding: 2px 16px 2px 0;border-bottom: 1px solid #9e9e9e;}
            table thead th {border-width: 2px;}
            .green{color: green;}
            .red{color: red;}
            .container {margin: 0 auto;width: 70%;}
            .analyzed, .coverage, .hit, .missed {padding: 10px;margin-bottom: 20px;border-radius: 6px;}
            .analyzed{ background: #bde6ff;}
            .coverage{ background: #ececec;}
            .hit{ background: #beffc1; }
            .hit table td, .hit table th{ border-color: #5d925f;}
            .missed{ background: #ffc9c9;}
            .missed table td, .missed table th{ border-color: #804646;}
        </style>
    </head>
    <body>
    <div class='container'>
        <h1>Pester Coverage Report</h1>
        <!-- CI Meta -->
        <ul>
            <li><strong>Build:</strong> $($BuildNumber)</li>
            <li><strong>Repo:</strong> $($Repository)</li>
            <li><strong>Generated on:</strong> $($Date)</li>
        </ul>
        <!-- Overview -->
        <div class='coverage'>
            <h2>Coverage: <span class='$($BuildColor)'>$($CoveragePercent) %</span></h2>
            <table>
                <thead>
                    <tr>
                        <th>Metric</th>
                        <th>Value</th>
                    </tr>
                </thead>
                <tr>
                    <td>Commands Analyzed</td>
                    <td>$($CommandsAnalyzed)</td>
                </tr>
                <tr>
                    <td>Files Analyzed</td>
                    <td>$($FilesAnalyzed)</td>
                </tr>
                <tr>
                    <td>Commands Executed</td>
                    <td>$($CommandsExecuted)</td>
                </tr>
                <tr>
                    <td>Commands Missed</td>
                    <td>$($CommandsMissed)</td>
                </tr>
            </table>
        </div>
        <div class='analyzed'>
            <h2>Files Analyzed: $($FilesAnalyzed)</h2>
            <ul>$($AnalyzedFiles)</ul>
        </div>
        <div class='hit'>
            <h2>Hit: $($CommandsExecuted)</h2>
            $($HitTable)
        </div>
        <div class='missed'>
            <h2>Missed: $($CommandsMissed)</h2>
            $($MissedTable)
        </div>
    </div>
    </body>
    </html>
    ";

    Set-Content -Path $OutputFile -Value $html
}

function Invoke-PSTestReport
{
    [CmdletBinding()]
    Param(
        [int]     $BuildNumber = 0,
        [string]  $GitRepo = "Repo Name",
        [string]  $GitRepoURL = "#",
        [string]  $CiURL = "#",
        [boolean] $ShowHitCommands = $false,
        [double]  $Compliance = 0.8,
        [string]  $ScriptAnalyzerFile = ".\artifacts\ScriptAnalyzerResults.json",
        [string]  $PesterFile = ".\artifacts\PesterResults.json",
        [string]  $OutputDir = ".\artifacts",
        [switch]  $FailOnSkippedOrPending
      )
      
      $ReportFolder = '.\TestReport'

      # HTML Colors
      $Colors = @{
          GREEN = '#5cb85c'
          YELLOW = '#FFCE56'
          RED = '#FF6384'
      }
      
      # Load HTML Template
      $templateFile = "$ReportFolder\TestReport.htm"
      
      # Load Pester JSON Files: Required
      if (!(Test-Path $PesterFile)) 
      { 
          throw "$PesterFile is missing."
      }
      $Pester = Get-Content $PesterFile | ConvertFrom-Json
      
      # Load ScriptAnalyzer JSON Files: Optional
      $ScriptAnalyzer = $null;
      if ((Test-Path $ScriptAnalyzerFile)) 
      { 
          $ScriptAnalyzer = Get-Content $ScriptAnalyzerFile | ConvertFrom-Json
      }
      
      # Copy Dependencies manually for now
      Copy-Item -Recurse -Path "$ReportFolder" -Destination (Join-Path $OutputDir "lib") -Force
      
      # From Pester Source
      function Get-HumanTime($Seconds) 
      {
          if($Seconds -gt 0.99) {
              $time = [math]::Round($Seconds, 2)
              $unit = 's'
          }
          else {
              $time = [math]::Floor($Seconds * 1000)
              $unit = 'ms'
          }
          return "$time$unit"
      }
      
      # Parse Git Commit
      function Get-GitCommitHash($Length)
      {
          $sha = $null
          
          try {
              $sha = (. git rev-parse HEAD).Substring(0, $length)
          }
          catch [System.Exception] {
              Write-Verbose "Git Hash could not be retrieved."
          }
          
          return $sha
      }
      
      # Create Test HTML Table
      #$TestResults = $Pester.TestResult | Select-Object Result, Name, Describe, Context, FailureMessage, Time
      foreach($test in $Pester.TestResult)
      {
          if($test.Result -eq "Passed")
          {
              $status = "<span class='label label-success'>Passed</span>"
          }
          elseif($test.Result -eq "Skipped")
          {
              $status = "<span class='label label-warning'>Skipped</span>"
          }
          elseif($test.Result -eq "Pending")
          {
              $status = "<span class='label label-info'>Pending</span>"
          }
          else
          {
              $status = "<span class='label label-danger'>Failed</span>"
          }
      
          #$formatTime = [String]::format("{0:%m}m {0:%s}s {0:%ffff}ms", [TimeSpan] $test.Time)
      
          $formatTime =  Get-HumanTime $test.Time.TotalSeconds
      
          $TestResultsTable += "
              <tr>
                  <td>$status</td>
                  <td>$($test.Name)</td>
                  <td>$($test.Describe)</td>
                  <td>$($test.Context)</td>
                  <td>$($test.FailureMessage)</td>
                  <td>$formatTime</td>
              </tr>
          "
      }
      
      # Create Script Analyzer Table
      foreach ($issue in $ScriptAnalyzer)
      {
          if($issue.Severity -eq "1")
          {
              $status = "<span class='label label-warning'>Warning</span>"
          }
          else
          {
              $status = "<span class='label label-danger'>Error</span>"
          }
      
          $ScriptAnalysisTable += "
          <tr>
              <td>$status</td>
              <td>$($issue.RuleName)</td>
              <td>$($issue.ScriptName)</td>
              <td>$($issue.Line)</td>
              <td>$($issue.Column)</td>
              <td>$($issue.Message)</td>
          </tr>
          "
      }
      
      #Keep track of single file coverage so we only iterate once
      $FileCoverage = @{}
      
      foreach($file in $Pester.CodeCoverage.AnalyzedFiles) 
      {
          $FileCoverage[$file] = @{
              Hit = 0
              Missed = 0
          }
      }
      
      # Create Script for Coverage Table (missed)
      Add-Type -AssemblyName System.Web
      foreach($missed in $Pester.CodeCoverage.MissedCommands)
      {
          $command = [System.Web.HttpUtility]::HtmlEncode($missed.Command)
      
          $CoverageTable += "
              <tr>
                  <td><span class='label label-warning'>Missed</span></td>
                  <td>$($missed.File)</td>
                  <td>$($missed.Line)</td>
                  <td>$($missed.Function)</td>
                  <td><pre style='border:1px solid $($Colors.YELLOW); background-color: transparent;'>$($command)</pre></td>
              </tr>
          "
      
          $FileCoverage[$missed.File].Missed++
      }
      
      # Create Script for Coverage Table (hit) 
      foreach($hit in $Pester.CodeCoverage.HitCommands)
      {
          if($ShowHitCommands)
          {
              $command = [System.Web.HttpUtility]::HtmlEncode($hit.Command)
      
              $CoverageTable += "
                  <tr>
                      <td><span class='label label-success'>Hit</span></td>
                      <td>$($hit.File)</td>
                      <td>$($hit.Line)</td>
                      <td>$($hit.Function)</td>
                      <td><pre style='border:1px solid $($Colors.GREEN); background-color: transparent;'>$command</pre></td>
                  </tr>
              "
          }
      
         $FileCoverage[$hit.File].Hit++
      }
      
      # Create File Tested Table
      # TODO: Add coverage % calcs for each file?
      foreach($file in $FileCoverage.GetEnumerator())
      {
          $total = ($file.Value.Hit + $file.Value.Missed)
          $coverage = [Math]::Round(($file.Value.Hit / $total) * 100, 2)
          
          if($coverage -lt 10)
          {
             $color = "progress-bar-danger"
          }
          elseif($coverage -lt 50)
          {
              $color = "progress-bar-warning"
          }
          else
          {
              $color = "progress-bar-success"
          }
      
          $FilesTestedTable += "
              <tr>
              <td>
                  <span>$($file.Name)</span>
                  <div class='progress' style='margin-bottom:0;'>
                      <div class='progress-bar $color' role='progressbar' aria-valuenow='$coverage' aria-valuemin='0' aria-valuemax='100' style='width: $coverage%; min-width: 2em;'>$coverage %</div>
                  </div>
              </td>
              </tr>
          "
      }
      
      $OverallCoverage = ($Pester.CodeCoverage.NumberOfCommandsExecuted/$Pester.CodeCoverage.NumberOfCommandsAnalyzed)
      
      
      # Determine Pester overall status
      if($Pester.FailedCount -eq 0)
      {
          # If the switch $FailOnSkippedOrPending is set, any skipped or pending tests will fail the build.
          if ($FailOnSkippedOrPending -and ($Pester.PendingCount -ne 0) -and ($Pester.SkippedCount -ne 0) ) 
          {
              $PesterPassed = $false
          }
          else
          {
              $PesterPassed = $true
          }
      }
      else
      {
          $PesterPassed = $false
      }
      
      # Replace Everything in html template and output report
      $Replace = @{
          # Custom
          REPO = $GitRepo
          REPO_URL = $GitRepoURL
          CI_BUILD_URL = $CiURL
          NUMBER_OF_PS1_FILES = (Get-ChildItem -Recurse -Include *ps1 | Measure-Object).Count
          BUILD_NUMBER = $BuildNumber
          BUILD_DATE = Get-Date -Format u
          COMMIT_HASH = Get-GitCommitHash -Length 10
      
          # Generated 
          BUILD_RESULT = if($OverallCoverage -ge $Compliance -and $PesterPassed) {"PASSED"} else {"FAILED"}
          BUILD_RESULT_COLOR = if($OverallCoverage -ge $Compliance -and$PesterPassed) {"panel-success"} else {"panel-danger"}
          BUILD_RESULT_ICON = if($OverallCoverage -ge $Compliance -and $PesterPassed) {"fa-check"} else {"fa-times"}
      
          TEST_TABLE = $TestResultsTable
          FILES_TESTED_TABLE = $FilesTestedTable
          SCRIPT_ANALYSIS_TABLE = $ScriptAnalysisTable
          COVERAGE_TABLE = $CoverageTable
          TEST_OVERALL = [Math]::Round(($Pester.PassedCount/$Pester.TotalCount) * 100, 0)
          TEST_OVERALL_COLOR = if($Pester.FailedCount -eq 0) {$Colors.GREEN} else {$Colors.RED}
          COVERAGE_OVERALL = [Math]::Round($OverallCoverage * 100, 0)
          COVERAGE_OVERALL_COLOR = if($OverallCoverage -lt 0.1) {$Colors.RED} elseif($OverallCoverage -lt 0.5) {$Colors.YELLOW} else {$Colors.GREEN} 
      
          # Pester
          TOTAL_COUNT = $Pester.TotalCount
          PASSED_COUNT = $Pester.PassedCount
          FAILED_COUNT = $Pester.FailedCount
          SKIPPED_COUNT = $Pester.SkippedCount
          PENDING_COUNT = $Pester.PendingCount
          NUMBER_OF_COMMANDS_ANALYZED = $Pester.CodeCoverage.NumberOfCommandsAnalyzed
          NUMBER_OF_FILES_ANALYZED = $Pester.CodeCoverage.NumberOfFilesAnalyzed
          NUMBER_OF_COMMANDS_EXECUTED = $Pester.CodeCoverage.NumberOfCommandsExecuted
          NUMBER_OF_COMMANDS_MISSED = $Pester.CodeCoverage.NumberOfCommandsMissed
      
          # Script Analyzer
          SA_ERROR_COUNT = ($ScriptAnalyzer | where-object {$_.Severity -eq "2"} | Measure-Object).Count
          SA_WARNING_COUNT = ($ScriptAnalyzer | where-object {$_.Severity -eq "1"} | Measure-Object).Count
      }
      
      $template = (Get-Content $templateFile) 
      foreach ($var in $Replace.GetEnumerator())
      {
          # Write-Host "Replacing: $($var.key)"
          $template = $template | ForEach-Object { $_.replace( "{$($var.key)}", $var.value ) }  
      }
      
      $template | Set-Content (Join-Path $OutputDir "TestReport.htm")
}